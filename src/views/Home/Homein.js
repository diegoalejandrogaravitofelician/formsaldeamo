import React, { useState } from "react";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import FromOne from "../../components/FormServices/FormOne";
import FromTwo from "../../components/FormServices/FormTwo";
import FormThree from "../../components/FormServices/FormThree";
import Result from "../../components/FormServices/Result";
function Homein() {

    const [ValidarForm, setValidarForm] = useState(false)
    const [controCol, setControlCol] = useState(12)
    let [contador, setContador] = useState(0)
    const [validarbtn, setvalidarbtn] = useState(true)
    const [disablechek, setdisablecheck]=useState(false)
    const [result, setResult]=useState(false)
    const [Vchecks, setVChecks] = useState({
        checkuno: false,
        checkdos: false,
        checktres: false
    })

    const [objeData ,setobjeData] =useState({
            text : '',
            fecha : '',
            cantidad : ''
    })

    const onChange = (e) =>{
        console.log("ingreso")
        console.log(e.target.name);

        setobjeData({
            ...objeData,
            [e.target.name] : e.target.value
        })

        console.log(objeData);

    }

    const fecha = (value ,e) =>{
        setobjeData({
            ...objeData,
            fecha: value
        })
        
    }


    const validarChecks = (e) => {
        console.log(e.target.checked);
        console.log(e.target.name);
        setVChecks({
            ...Vchecks,
            [e.target.name]: e.target.checked
        })
        if (e.target.name === "checkuno") {
            if (e.target.checked === true) {
                setContador(contador + 1);
            } else {
                setContador(contador - 1);
            }

        } else if (e.target.name === "checkdos") {
            if (e.target.checked === true) {
                setContador(contador + 1);
            } else {
                setContador(contador - 1);

            }

        } else if (e.target.name === "checktres") {
            if (e.target.checked === true) {
                setContador(contador + 1);
            } else {
                setContador(contador - 1);
            }
        }

    }
    const validarFroms = () => {

        setValidarForm(true)
        setvalidarbtn(false)
        setdisablecheck(true)
        if (contador === 2) {
            setControlCol(6)
        } else if (contador === 3) {
            setControlCol(4)
        } else if (contador === 1) {
            setControlCol(12)
        }

    }

    const validarResultado = ()=>{
        setResult(true)
        setValidarForm(true)

    }
    return (
        <>
            <Container className="mt-3">
                <Row className="border border-dark">

                    <Col lg="12">
                        <div className="pt-3">
                            <h3> Selecciona los campos que tendrán el formulario </h3>
                        </div>

                    </Col>

                    <Col lg="2" className="text-center mt-4">
                        <div className="custom-control custom-checkbox checkbox-lg">
                            <input type="checkbox" className="custom-control-input" id="checkuno" disabled={disablechek} name="checkuno" onChange={(e) => validarChecks(e)} />
                            <label className="custom-control-label" for="checkuno">Texto</label>
                        </div>
                    </Col>
                    <Col lg="2" className="text-center mt-4">
                        <div className="custom-control custom-checkbox checkbox-lg">
                            <input type="checkbox" className="custom-control-input" id="checkdos" disabled={disablechek} name="checkdos" onChange={(e) => validarChecks(e)} />
                            <label className="custom-control-label" for="checkdos">Fecha</label>
                        </div>
                    </Col>
                    <Col lg="2" className="text-center mt-4">
                        <div className="custom-control custom-checkbox checkbox-lg">
                            <input type="checkbox" className="custom-control-input" id="checktres" disabled={disablechek} name="checktres" onChange={(e) => validarChecks(e)} />
                            <label className="custom-control-label" for="checktres">Rango</label>
                        </div>
                    </Col>

                    <Col lg={12} className="mt-2 mb-4">
                        {validarbtn && (
                            <Button id="btncrearForm" variant="primary" className="float-right" onClick={() => validarFroms()}>Crear</Button>

                        )}
                    </Col>
                </Row>
            </Container>
            {ValidarForm && (
                <Container className="mt-3">
                    <Row className="border border-dark">

                        <Col lg={controCol} className="mt-4">
                            {Vchecks.checkuno && (
                                <>
                                    <FromOne onChange={onChange} />
                                </>
                            )}

                        </Col>
                        <Col lg={controCol} className="mt-4">
                            {Vchecks.checkdos && (
                                <>

                                    <FormThree  fecha={fecha}/>
                                </>
                            )}
                        </Col>
                        <Col lg={controCol} className="mt-4">
                            {Vchecks.checktres && (
                                <>
                                    <FromTwo onChange={onChange}/>
                                </>
                            )}
                        </Col>

                        <Col lg={12} className="mt-4">
                            {result && (
                                <>
                                    <Result data={objeData}/>
                                </>
                            )}
                        </Col>

                        <Col lg={12} className="mt-2 mb-4">
                            <Button variant="primary" className="float-right" onClick={()=> validarResultado()}>Enviar</Button>
                        </Col>
                    </Row>
                </Container>

            )}

        </>
    )
}

export default Homein;