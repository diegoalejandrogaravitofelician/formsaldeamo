import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Homein from './views/Home/Homein';

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Homein />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
