import React, { useEffect, useState } from "react";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

function Result(props) {

    console.log(props)
    const {data} = props;
    console.log(data)
    const [datainforme, setdatainforme]=useState(data.data);
    useEffect(()=>{
        setdatainforme(JSON.stringify(data))
    },[])



    return (

        <Container className="mt-3">
            <Row className="border border-dark">

                <Col lg="12" className="mb-5">
                    <div className="pt-3 mt-2 mb-5">
                        <h5> Datos del formulario </h5>
                    </div>

                    <p>{datainforme}</p>


                </Col>

            </Row>
        </Container>
    )
}

export default Result;