import React from "react";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

function FromOne(props) {

    const {onChange} = props;

    return (
        <Container className="mt-3">
            <Row className="border border-dark">

                <Col lg="12">
                    <div className="pt-3 mt-2 mb-5">
                        <h5> Digite el texto </h5>
                    </div>
                    <Form.Control
                        name="text"
                        as="textarea"
                        style={{ height: '100px' }}
                        className="mb-4 border border-dark"
                        onChange={(e)=>onChange(e)}
                    />

                </Col>


            </Row>
        </Container>
    )
}

export default FromOne;