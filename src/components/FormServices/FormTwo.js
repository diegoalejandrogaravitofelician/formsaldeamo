import React from "react";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

function FromTwo(props) {
    const {onChange} = props;
    return (

        <Container className="mt-3">
            <Row className="border border-dark">

                <Col lg="12" className="mb-5">
                    <div className="pt-3 mt-2 mb-5">
                        <h5> Seleccione el valor </h5>
                    </div>
                    <div className="mb-5 text-center">
                        <input type="range" name="cantidad" min="1" max="10" 
                        onChange={(e)=>onChange(e)}
                        />
                        <Col lg={9} className="mt-3 ml-5 mb-5">
                            <span className="float-left ml-5" >1</span>
                            <span className="float-right">10</span>
                        </Col>
                    </div>




                </Col>


            </Row>
        </Container>
    )
}

export default FromTwo;