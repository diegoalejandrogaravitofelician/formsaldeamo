import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

function FormThree(props) {

    const [startDate, setStartDate] = useState(new Date());
    const {fecha} = props;

    return (
        <Container className="mt-3">
            <Row className="border border-dark">

                <Col lg="12" className="mb-5">
                    <div className="pt-3 mt-2 mb-5">
                        <h5> Seleccione la fecha </h5>
                    </div>
                    <div className="mb-5 text-center">
                        <DatePicker
                            name="valor"
                            id="valor"
                            selected={startDate}
                            onChange={(value, e) => setStartDate(value) + fecha(value, e)}
                            

                        />

                    </div>




                </Col>


            </Row>
        </Container>
    )
}

export default FormThree;